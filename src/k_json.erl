-module(k_json).

-export([encode/1, decode/1, get_value/2, get_value/3]).

-type json_proplist() :: [ {binary(), k_json() } ].
-type jsonable_proplist() :: [ {binary() | atom(), any() } ].

-type k_hashable() :: {struct, jsonable_proplist()} | jsonable_proplist().

-type k_json() :: {struct, json_proplist() } | [ k_json() ] 
		| integer() | binary() | true | false | null.
-type k_jsonable() :: {struct, jsonable_proplist() } | [ k_json() ] 
		| integer() | binary() | true | false | null.

-export_type([k_json/0]).

encode(Json) ->
    mochijson2:encode(Json).

-spec decode( binary() ) -> k_json().
decode(Json) ->
    mochijson2:decode(Json).

-spec get_value(binary() | string(), List :: k_hashable()) -> any().
get_value(Key, List) ->
    get_value(Key, List, undefined).

-spec get_value(binary() | string(), List :: k_hashable(), Def :: any()) -> any().
get_value(Key, {struct,List}, Def) ->
  get_value( Key, List, Def );
get_value(Key, List, Def) when is_binary( Key )->
  proplists:get_value(Key, List, Def);
get_value(Key, List, Def) when is_list(Key)->
  proplists:get_value(list_to_binary(Key), List, Def);
get_value(Key, List, Def)->
  proplists:get_value(Key, List, Def).

%%
%% Tests
%%
-ifdef(TEST).
-include_lib("eunit/include/eunit.hrl").

get_value_helper(List)->
  1 = get_value(a, List),
  undefined = get_value(<<"a">>, List),
  
  undefined = get_value(b, List),
  2 = get_value(<<"b">>, List),
  2 = get_value("b", List),

  undefined = get_value(c, List),
  undefined = get_value("c", List),
  undefined = get_value(<<"c">>, List),

  undefined = get_value(d, List),
  undefined = get_value("d", List),
  undefined = get_value(<<"d">>, List),
  ok.

get_value_test() ->
  List = [{a,1},{<<"b">>,2},{"c",3}],
  get_value_helper(List),
  get_value_helper({struct,List}),
  ok.
-endif.

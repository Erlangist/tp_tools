-module(k_tools).
-author('serge.osnach@gmail.com').

-export([trim/1]).
-export([bin/1,list/1,  max/2, to_int_def/2, 
    to_float/1, neg/1, 
    ceiling/1, divide/2,
    shuffle/1, load_term/1, bin_to_term/1, term_to_bin/1, jsonify/1, atom/1,
    insert_between/2, insert_between_nth/3,
    get_env/3, ensure_started/1, gen_call/1]).
-export([keys_to_bin/1, append_to_value/3, get_value/2, set_value/3, remove_key/2, get_values/1]).
-export([utf_bin/1]).

-export([is_app_running/1]).
-export([append_file/2]).
-export([split_list/2]).
-export([remove_duplicates/1]).

-type proplist() :: [{ any(), any() }].
-type json_hash() :: {struct, proplist()}.
-type gen_list() :: proplist() | json_hash().
-type binarable() :: binary() | list() | atom().

neg( X ) when is_atom(X) -> X;
neg( X ) -> -X.

-spec bin( binarable() ) -> binary().
bin( X ) when is_list( X ) -> list_to_binary( X );
bin( X ) when is_atom( X ) -> atom_to_binary( X, latin1 );
bin( X ) %when is_binary( X ) 
  -> X.

atom( X ) when is_atom(X) -> X;
atom( X ) when is_binary( X ) -> binary_to_atom( X, latin1 );
atom( X ) when is_list( X ) -> list_to_atom( X );
atom( X ) -> X.


list( X ) when is_atom( X ) -> atom_to_list( X );
list( X ) when is_binary( X ) -> binary_to_list(X);
list( X ) when is_list( X ) -> X.

max( A, B ) when is_atom(A) -> B;
max( A, B ) when is_atom(B) -> A;
max( A, B )                 -> %io:format("((~p ~p))", [A,B]),
              erlang:max( A, B ).

to_int_def( Str, Def ) when is_binary( Str )->
    to_int_def( binary_to_list(Str), Def );
to_int_def( Str, _Def ) when is_integer( Str )->
    Str;
to_int_def( Str, Def ) ->
    case string:to_integer( Str ) of
        {error, _Reason} -> Def;
        { Ret, _Rest } -> Ret
    end.

to_float(S) when is_binary(S)->
    to_float( binary_to_list(S));
to_float(S) when is_float(S)->
    S;
to_float(S) ->
    case string:to_float(S) of
        {error, no_float} ->
            list_to_integer(S) * 1.0;
        {X,_More} -> X
    end.


-spec get_value(binarable(), gen_list()) -> any().
get_value(Key, {struct,List}) ->
  get_value( Key, List );
get_value(Key, List) when is_binary( Key )->
  proplists:get_value(Key, List, proplists:get_value(binary_to_list(Key), List));
get_value(Key, List) when is_list(Key)->
  proplists:get_value(Key, List, proplists:get_value(list_to_binary(Key), List));
get_value(Key, List)->
  proplists:get_value(Key, List).


-spec set_value(binarable(), any(), gen_list()) -> gen_list().
set_value(Key,Val,{struct,List}) -> {struct,set_value(Key,Val,List)};
set_value(Key,Val,List) when is_list(List) ->
  set_value2(Key,Val,List,[]).

set_value2(Key,Val,[],Ret) ->
  lists:reverse([{Key,Val}|Ret]);
set_value2(Key,Val,[{Key,_OldVal}|Rest],Ret) ->
  set_value3([{Key,Val}|Rest], Ret);
set_value2(Key,Val,[El|Rest],Ret) ->
  set_value2(Key,Val,Rest, [El|Ret]).

set_value3(L,[El|Rest]) -> set_value3([El|L],Rest);
set_value3(L,[]) -> L.

ceiling(X) ->
    T = erlang:trunc(X),
    case (X - T) of
        Neg when Neg < 0 -> T;
        Pos when Pos > 0 -> T + 1;
        _ -> T
    end.

%%
%% Shuffling the list
%%
shuffle(List) ->
    {A1,A2,A3} = now(),
    random:seed(A1, A2, A3),
%% Determine the log n portion then randomize the list.
   randomize(round(math:log(length(List)) + 0.5), List).

randomize(1, List) ->
   randomize(List);
randomize(T, List) ->
   lists:foldl(fun(_E, Acc) ->
                  randomize(Acc)
               end, randomize(List), lists:seq(1, (T - 1))).

randomize(List) ->
   D = [{random:uniform(), A} || A <- List],
   {_, D1} = lists:unzip(lists:keysort(1, D)),
   D1.
   
%%
%% Load term from file
%%
load_term(FileName) ->
  {ok, File} = file:read_file(FileName),
  bin_to_term(File).

bin_to_term(Str) ->
  {ok, A, _ } = erl_scan:string(binary_to_list(Str)),
  %A = erl_scan:tokens([], binary_to_list(File), {1,1}),
  {ok, Term} = erl_parse:parse_term(A),
  Term.

term_to_bin(X) ->
  iolist_to_binary(io_lib:format("~w.", [X])).

jsonify( X ) when is_list( X ) -> list_to_binary( X );
jsonify( X ) when is_atom( X ) -> X;
jsonify( {datetime, {{Y,M,D}, _}}) -> iolist_to_binary(io_lib:format("~p-~p-~p",[Y,M,D]));
jsonify( X ) -> X.

-spec is_app_running( atom() ) -> {term(), term(), term() } | false.
is_app_running( AppName )->
  case lists:keyfind(AppName,1,application:which_applications()) of
    {Name, Desc, Ver} ->
      {Name, Desc, Ver};
    _ -> false
  end.

divide(X,_) when is_atom(X) -> X;
divide(X,Y) -> X/Y.

insert_between([], _InsertElement, Acc) ->
  Acc;
insert_between([Value | OtherValues], InsertElement, []) ->
  insert_between(OtherValues, InsertElement, [Value]);
insert_between([Value | OtherValues], InsertElement, Acc) ->
  Acc1 = [Value, InsertElement | Acc],
  insert_between(OtherValues, InsertElement, Acc1).
insert_between(ValuesList, InsertElement) ->
  lists:reverse(insert_between(ValuesList, InsertElement, [])).

insert_between_nth([], _InsertElement, _Count, _Limit, Acc) ->
  Acc;
insert_between_nth(ValuesList, InsertElement, 0, Limit, Acc) ->
  insert_between_nth(ValuesList, InsertElement, Limit, Limit, [InsertElement|Acc]);
insert_between_nth([Val|ValuesList], InsertElement, Count, Limit, Acc) ->
  insert_between_nth(ValuesList, InsertElement, Count-1, Limit, [Val|Acc]).

insert_between_nth(ValuesList, InsertElement, Nth) ->
  lists:reverse(insert_between_nth(ValuesList, InsertElement, Nth, Nth, [])).

get_env(App, Key, Default) ->
  case application:get_env(App, Key) of
  {ok, Value} -> Value;
  _ -> Default
  end.

ensure_started(App) ->
    case application:start(App) of
        ok ->
            ok;
        {error, {already_started, App}} ->
            ok;
        Other ->
            Other
    end.

gen_call(Fun) when is_function(Fun, 0) ->
  Fun();
gen_call({Mod, Fun, Args}) ->
  apply(Mod, Fun, Args).

-spec keys_to_bin(gen_list()) -> gen_list().
keys_to_bin({struct,L}) -> {struct,keys_to_bin(L)};
keys_to_bin(L) when is_list(L) -> [key_to_bin(X) || X <- L].

-spec key_to_bin({binarable(),any()}) -> {binary(),any()}.
key_to_bin({K,V}) -> {bin(K),V};
key_to_bin(Other) -> Other.


append_to_value({struct,List},Key,Value) ->
  {struct,append_to_value(List,Key,Value)};
append_to_value(List,Key,Value) ->
  append_to_value(List,Key,Value, []).

append_to_value([],Key,Value,Acc) -> lists:reverse([{Key,[Value]}|Acc]);
append_to_value([{Key,V}|Rest],Key,Value,Acc) ->
  %append_to_value(Rest,Key,Value,[{Key,[Value|V]}|Acc]);
  lists:reverse([{Key,[Value|V]}|Acc])++Rest;
append_to_value([El|Rest],Key,Value,Acc) ->
  append_to_value(Rest,Key,Value,[El|Acc]).
  

remove_key(Key,{struct,List}) ->
  {struct,remove_key(Key,List)};
remove_key(Key,List) ->
  remove_key(Key,List,[]).
  
remove_key(_Key,[],Acc) -> lists:reverse(Acc);
remove_key(Key,[{Key,_Val}|Rest],Acc) -> remove_key(Key,Rest,Acc);
remove_key(Key,[I|Rest],Acc) -> remove_key(Key,Rest,[I|Acc]).

get_values({struct, X}) -> get_values(X);
get_values(X) -> [Y || {_,Y} <- X].

trim(Subject) when is_binary(Subject)->
  %%trim(unicode:characters_to_list(Subject));
  trim(binary_to_list(Subject));
trim([]) -> [];
trim(Subject) ->
  Subjec2 = re:replace(Subject, "^\\s*", "", [unicode]),
  Subjec3 = re:replace(Subjec2, "\\s*$", "", [unicode]),
  k_tools:bin(Subjec3).

append_file(Filename, Bytes) ->
    case file:open(Filename, [append]) of
        {ok, IoDevice} ->
            file:write(IoDevice, Bytes),
            file:close(IoDevice);
        {error, Reason} ->
            io:format("~s open error  reason:~s~n", [Filename, Reason])
    end.

utf_bin(X) when is_binary(X) -> X;
utf_bin(X) ->
    case [ Y || Y <-X , Y > 255 ] of
	[] -> list_to_binary(X);
	_ -> unicode:characters_to_binary(X)
    end.

split_list(List, Max) ->
    element(1, lists:foldl(fun
        (E, {[Buff|Acc], 1}) ->
            {[[E],Buff|Acc], Max};
        (E, {[Buff|Acc], C}) ->
            {[[E|Buff]|Acc], C-1};
        (E, {[], _}) ->
            {[[E]], Max}
    end, {[], Max}, List)).


delete_all(Item, [Item | Rest_of_list]) ->
    delete_all(Item, Rest_of_list);
delete_all(Item, [Another_item| Rest_of_list]) ->
    [Another_item | delete_all(Item, Rest_of_list)];
delete_all(_, []) -> [].

remove_duplicates(List)-> removing(List,[]).
removing([],This) -> lists:reverse(This);
removing([A|Tail],Acc) -> 
    removing(delete_all(A,Tail),[A|Acc]).

%%
%% Tests
%%
-ifdef(TEST).
-include_lib("eunit/include/eunit.hrl").

trim_test() ->
  <<"">> = trim("  \t  "),
  <<"a b">> = trim(" a b "),
  <<"a __">> = trim(" \na __"),
  U = [1040,1074,1090,1086,1082,1088,1077,1089,1083,1072],
  U = trim(U),
  ok.

-endif.
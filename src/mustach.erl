-module(mustach).

-export([compile/2, render/3]).

-record(p_state, {stack=[],partial_h=undefined}).
-record(r_state, {opts=[], stack=[], partials=[], partial_h = undefined}).

compile(Tmpl,Opts) when is_binary(Tmpl) ->
  parse(Tmpl,[],Opts);
compile(Tmpl,Opts) when is_list(Tmpl) ->
  compile(list_to_binary(Tmpl),Opts).

parse(Tmpl, Ret,Opts)->
  Partial_h = proplists:get_value(partials,Opts,undefined),
  parse(#p_state{ partial_h = Partial_h}, Tmpl,<<>>, Ret).

parse(_State,<<>>,Current,Ret) ->
  Ret2 = [make_raw(Current)|Ret],
  lists:reverse( Ret2 );
parse(State,<< ${, ${, Rest/binary >>,Current,Ret) ->
  parse_open_tag(State,Rest, [make_raw(Current)|Ret]);
parse(State, <<A,Rest/binary >>,Current,Ret) ->
  parse(State,Rest,<<Current/binary, A>>, Ret).


make_raw(Text) -> {raw, Text}.

parse_open_tag(State,<< $/, Rest/binary>>, Compiled) ->
  {NewText,_Name} = parse_get_name(Rest),
  Stack = State#p_state.stack,
  [_Name|Stack2] = Stack,
  %parse(State#p_state{stack = Stack2}, NewText, <<>>, [{if_block, Name}|Compiled]).
  {NewText,lists:reverse(Compiled)};
parse_open_tag(State,<< $#, Rest/binary>>, Compiled) ->
  {NewText,Name} = parse_get_name(Rest),
  Stack = State#p_state.stack,
  Stack2 = [Name|Stack],
  {NewText2,Inner} = parse(State#p_state{stack = Stack2}, NewText, <<>>, []),
  parse(State,NewText2,<<>>, [{if_block, Name, Inner}|Compiled]);
parse_open_tag(State,<< $^, Rest/binary>>, Compiled) ->
  {NewText,Name} = parse_get_name(Rest),
  Stack = State#p_state.stack,
  Stack2 = [Name|Stack],
  {NewText2,Inner} = parse(State#p_state{stack = Stack2}, NewText, <<>>, []),
  parse(State,NewText2,<<>>, [{ifnot_block, Name, Inner}|Compiled]);
parse_open_tag(State,<< $>, Rest/binary>>, Compiled) ->
  {NewText,[Name]} = parse_get_name(Rest),
  Partialer = State#p_state.partial_h,
  Partial = Partialer(Name),
  PartialC = parse(State,Partial,<<>>, []),
  parse(State,NewText,<<>>, [PartialC|Compiled]);
parse_open_tag(State, << ${, Text/binary >>, Compiled) ->
  {NewText,Name} = parse_get_name(Text),
  %%io:format("Tag ~p~n", [Name]),
  parse(State,NewText, <<>>, [{var_i, Name}|Compiled]);
parse_open_tag(State, Text, Compiled) ->
  {NewText,Name} = parse_get_name(Text),
  %%io:format("Tag ~p~n", [Name]),
  parse(State,NewText, <<>>, [{var, Name}|Compiled]).

parse_get_name(Text) -> parse_get_name(Text, <<>>).


parse_get_name(<< $}, $}, $}, Rest/binary>>, Name) ->
  {Rest, binary:split(Name, <<$.>>, [global])};
parse_get_name(<< $}, $}, Rest/binary>>, Name) ->
  {Rest, binary:split(Name, <<$.>>, [global])};
parse_get_name(<< S, Rest/binary>>, Name) ->
  parse_get_name( Rest, <<Name/binary, S>>).
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

render(Compiled, Data, Opts) ->
  Partial_h = proplists:get_value(partials,Opts,undefined),
  State = #r_state{ stack = [{ctx, Data}], partial_h=Partial_h},
  render2(Compiled, State, []).

render2([X|Rest], State, Result)->
  Part = render_part(X, State),
  render2(Rest, State, [Part|Result]);
render2([], _State, Result)->
  lists:reverse(Result).

render_part({raw, Raw}, _State) ->
  Raw;
render_part({var, VarName}, State) ->
  Raw = get_var_value( VarName, State ),
  make_displayable(Raw);
render_part({var_i, VarName}, State) ->
  Raw = get_var_value( VarName, State ),
  make_displayable_i(Raw);
render_part({if_block, VarName,Partial}, State) ->
  List = get_var_value( VarName, State ),
  case List of
    undefined -> <<>>;
    false -> <<>>;
    true -> render_part(Partial,State);
    Tuple when is_tuple(Tuple) ->
      Stack = State#r_state.stack,
      State2 = State#r_state{ stack = [{ctx,Tuple}|Stack] },
      render_part(Partial,State2);
    <<>> -> <<>>;
    Bin when is_binary(Bin) ->
       render_list(Partial,State,[{raw,Bin}],[]);
	Int when is_integer(Int) ->
       render_list(Partial,State,[{raw,make_displayable(Int)}],[]);
    _Other ->
       render_list(Partial,State,List,[])
  end;
render_part({ifnot_block, VarName,Partial}, State) ->
  List = get_var_value( VarName, State ),
  case List of
    undefined -> render2(Partial,State,[]);
    false -> render_part(Partial,State);
    _Other -> <<>>
  end;
render_part(L,State) when is_list(L) ->
  render2(L,State,[]);
render_part(_, _State) ->
  Raw = "??",
  Raw.


render_list(Partial,State,[], Ret) ->
  lists:reverse(Ret);
render_list(Partial,State,[El|List], Ret) ->
  Stack = State#r_state.stack,
  State2 = State#r_state{ stack = [{ctx,El}|Stack] },
  Ret2 = render2( Partial, State2, [] ),
  render_list(Partial, State, List, [Ret2|Ret]).

get_var_value(Name, State) ->
  Stack = State#r_state.stack,
  Var = find_var_value(Name, Stack),
  %%io:format("get_var_value (~p) = ~p~n", [Name, Var]),
  Var.

find_var_value(_Name, []) ->
  undefined;
%%find_var_value(Name, [{list,Json,_Tail}|Stack]) ->
%%  case find_ctx_value(Name, Json) of
%%    undefined -> find_var_value( Name, Stack );
%%    Other -> Other
%%  end;
find_var_value(Name, [{ctx,Json}|Stack]) ->
  case find_ctx_value(Name, Json) of
    undefined -> find_var_value( Name, Stack );
    Other -> Other
  end.

find_ctx_value([], X) ->
  X;
find_ctx_value(_, {raw, _R}) ->
  undefined;
find_ctx_value([Name|Rest], {struct, PropList}) ->
  Val = proplists:get_value(Name,PropList, undefined),
  case Val of
     undefined -> undefined;
     _V -> find_ctx_value(Rest,Val)
  end;
find_ctx_value(OtherPath, OtherContext) ->
    lager:log(warning, self(), "bad data to mustache: ~p ~p", [OtherPath, OtherContext]),
    undefined.


make_displayable_i(undefined) ->
  <<>>;
make_displayable_i(Val) when is_function(Val,0) ->
  make_displayable_i(Val());
make_displayable_i(Val) when is_atom(Val) ->
  io_lib:format("~p", [Val]);
make_displayable_i(Val) when is_integer(Val) ->
  integer_to_list(Val);
make_displayable_i(Val) when is_float(Val) ->
  io_lib:format("~p", [Val]);
make_displayable_i(Val) ->
  Val.
  
make_displayable(undefined) ->
  <<>>;
make_displayable(Val) when is_function(Val,0) ->
  make_displayable(Val());
make_displayable(Val) when is_atom(Val) ->
  io_lib:format("~p", [Val]);
make_displayable(Val) when is_integer(Val) ->
  integer_to_list(Val);
make_displayable(Val) when is_float(Val) ->
  io_lib:format("~p", [Val]);
make_displayable(Val) when is_binary(Val)->
%%io:format("==--== ~p~n", [Val]),
  purify(Val, <<>>);
make_displayable(Val) ->
%%  io:format("==??== ~p~n", [Val]),
  Val.


purify(<<>>, Ret ) -> Ret;
purify(<< $>, Rest/binary>>, Ret ) -> purify( Rest, << Ret/binary, "&gt;">> );
purify(<< $<, Rest/binary>>, Ret ) -> purify( Rest, << Ret/binary, "&lt;">> );
purify(<< A, Rest/binary>>, Ret ) -> purify( Rest, << Ret/binary, A>> ).

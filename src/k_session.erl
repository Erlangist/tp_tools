-module(k_session).

-export([setup_session/1, setup_session/2, setup_session/3]).
-export([get_random_string/2]).

cookie_name_set() -> <<"SESSION">>.

get_random_string_weak(Length, AllowedChars) ->
    random:seed(now()),
    L = length(AllowedChars),
    [lists:nth(random:uniform(L), AllowedChars) || _X <- lists:seq(1, Length) ].

get_random_string_strong(Length, AllowedChars) ->
    L = length(AllowedChars),
    [lists:nth(crypto:rand_uniform(1,L+1), AllowedChars) || _X <- lists:seq(1, Length) ].

get_random_string(Length, AllowedChars) ->
    case application:get_application(crypto) of
    undefined ->
      get_random_string_weak(Length, AllowedChars);
    {ok, _} ->
      get_random_string_strong(Length, AllowedChars)
    end.

get_new_cookie() ->
  k_tools:bin(get_random_string(32,"abcdefghijklmnopqrstuvwxyz0123456789TRADEP.NET")).

setup_session(Req) ->
  setup_session(Req,cookie_name_set()).

setup_session(Req, CookieNameSet) ->
    setup_session(Req, CookieNameSet, false).

setup_session(Req, CookieNameSet, SetPath) ->
    CookieName = cowboy_bstr:to_lower(CookieNameSet),
    Session = case k_cowboy:cookie(CookieName, Req) of
                  {undefined, Req1} -> case k_cowboy:cookie(CookieNameSet, Req) of
                    {undefined, _Req1} -> undefined;
                    {Cook, _} -> Cook
                  end;
                  {B, Req1} when is_binary(B) -> B
              end,
    {Session1, Req3} = case Session of
      undefined ->
        SessionG = get_new_cookie(),
        %% max-age: 100 days
        CookieOpts = #{http_only =>  true, max_age => 100*24*60*60},
        CookieOpts2 = case SetPath of
           true -> [{path, <<"/">>} | CookieOpts];
           _ -> CookieOpts
        end,
        Req2 = cowboy_req:set_resp_cookie(CookieNameSet, SessionG, CookieOpts2, Req),
        {SessionG, Req2};
      _ -> {Session, Req}
      end,
      {Session1, Req}.
      
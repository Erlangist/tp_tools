-module(k_debug).

-export([mem/0]).

mem() ->
  mem(processes()).

mem(Ps) ->
  lists:foldl(fun(Pid,Acc) -> mem(Pid,Acc) end, [], Ps).

mem(Pid,Acc) ->
  {memory,X} = process_info(Pid,memory),
  if X > 10000 ->
    [{Pid,[X, process_info(Pid,registered_name)]}|Acc];
    true -> Acc
  end.


-module(k_proc).

-export([do_os_cmd/1]).

rloop( Port ) ->
    receive
        {Port, {data, Any}} ->
            io:format("~s", [Any] ),
            rloop(Port);
        {Port, _Other} ->
           ok
            %io:format("Other ~p~n", [Other] )
    end.

do_os_cmd( CmdLine ) ->
  Port = open_port( {spawn,CmdLine}, [stream, stderr_to_stdout, exit_status] ),
  rloop(Port).

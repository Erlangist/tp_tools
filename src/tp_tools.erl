-module(tp_tools).

-export([start/0,start/2]).

start() ->
    application:start(tp_tools).

start(_StartType, _StartArgs) ->
    {ok, self()}.
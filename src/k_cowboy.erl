-module(k_cowboy).

-export([gather2/1, cookie/2]).

cookie(Name, Req) ->
  Cookies = cowboy_req:parse_cookies(Req),
  case  lists:keyfind(Name, 1, Cookies) of
    {_, Lang} -> {Lang, Req};
    _ -> {undefined, Req}
  end.

gather2(Req0) -> gather2(Req0, []).
gather2(Req0, Acc) ->
    case cowboy_req:read_part(Req0) of
        {ok, Headers, Req1} ->
            case maps:get(<<"content-disposition">>,Headers, false) of
              false -> gather2(Req1, Acc);
              Value ->
		InsertVal="form-data; name=\"([^\"]*)\"(?:(?:.*)filename=\"(.*)\")?", % " ^
		case re:run(Value, InsertVal, [{capture, all_but_first, binary}]) of 	 
		  {match, [X, FileName]} ->
                        {ok, Body, Req2} = cowboy_req:read_part_body(Req1),
			gather2( Req2, [{X,Body}, {<<"filename">>, FileName}|Acc]); % FileName % 
		  {match, [X]} ->
                        {ok, Body, Req2} = cowboy_req:read_part_body(Req1),
			gather2( Req2, [{X,Body}|Acc]); % undefined
		  _ ->
			gather2( Req1, Acc)
		end
            end;
        {done, Req} ->
            {Req, Acc}
    end.
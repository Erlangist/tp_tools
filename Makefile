
PREFIX:=../
DEST:=$(PREFIX)$(PROJECT)

REBAR=./rebar

all:
	@$(REBAR) get-deps compile

edoc:
	@$(REBAR) doc

test:
	@rm -rf .eunit
	@mkdir -p .eunit
	@$(REBAR) skip_deps=true eunit

clean:
	@$(REBAR) clean

build_plt:
	@$(REBAR) build-plt

dialyzer:
	@$(REBAR) dialyze

app:
	@$(REBAR) create template=mochiwebapp dest=$(DEST) appid=$(PROJECT)

dia2:
	export ERL_TOP=/usr/lib/erlang
	dialyzer --build_plt -r --verbose --output_plt $(HOME)/.dialyzer_otp.plt $(ERL_TOP)/lib/stdlib-*/ebin $(ERL_TOP)/lib/kernel-*/ebin


dia:
	c